import pandas as pd
from datetime import datetime

def drop_columns(data):
  """
  Удаляет выбраные столбцы из датафрейма.
  """
  data = data.drop(['is_closed', 'track_code', 'can_access_closed'], axis = 1)
  return data


def rename_columns(data):
  """
  Переименовывает  столбцы в датафрейме.
  """
  data = data.rename(columns={
    'first_name': 'Имя',
    'last_name': 'Фамилия',
    'bdate': 'Дата рождения',
    'sex': 'Пол'})
  return data


def rename_values(data):
  """
  Заменяет значения в столбце.
  """
  data['Пол'] = data['Пол'].replace({1: 'Женский', 2: 'Мужской'})
  return data

def age(data):
    """
    - Преобразовывает столбец с датой рождения.
    - Вычисляет возраст.
    """
    data['Дата рождения'] = pd.to_datetime(data['Дата рождения'], errors='coerce')
    data['Возраст'] = (datetime.now() - data['Дата рождения']) // pd.Timedelta(days=365.25)
    return data