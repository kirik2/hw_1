import pytest
import pandas as pd
from functions import drop_columns
from functions import rename_columns
from functions import rename_values
from functions import age

def test_drop_columns(df):
    result_df = drop_columns(df)
    expected_columns =  ['id', 'bdate', 'sex',
                         'first_name', 'last_name']
    assert list(result_df.columns) == expected_columns

def test_rename_columns(df):
    result_df = rename_columns(df)
    expected_columns = ['id', 'Дата рождения', 'Пол', 'Имя', 'Фамилия']
    assert list(result_df.columns) == expected_columns

def test_rename_values(df):
    result_df = rename_values(df)
    assert result_df['Пол'].isin(['Женский', 'Мужской']).all()

def test_age(df):
    result_df = age(df)
    assert 'Возраст' in result_df.columns
